# Apply tax to item
def apply_taxes_to_article(price, porcentage_value)
  value = porcentage_value * price / 100
  value_round = (value * 20).ceil / 20.0
  value_round
end

# data reading based in type
def data_reading(type_gets)
  value_case = case type_gets
               when 'count'
                 gets.to_i
               when 'article'
                 gets.to_s
               else
                 gets.to_f
               end
  value_case
end

def analyze_data(arr_list)
  count = arr_list.count
  # Verify that the array has elements
  if count > 0
    sum_taxs = 0
    total = 0
    puts '----------------------------RESULT--------------------------------'
    # Loop through the array to see each element
    0.step(count - 1) do |i|
      hash_value = arr_list[i]
      taxs = 0
      count_value = hash_value[:count]
      article_value = hash_value[:article]
      price_value = hash_value[:price]

      # must be different from book, chocolates and pills to apply tax of 10%
      unless article_value.include?('book') || article_value.include?('pills') || article_value.include?('chocolates')
        tax_10 = apply_taxes_to_article(price_value, 10)
        sum_taxs += count_value * tax_10
        taxs += tax_10
      end

      # Apply tax of 5% in case of imported products
      if article_value.include?('imported')
        tax_5 = apply_taxes_to_article(price_value, 5)
        sum_taxs += count_value * tax_5
        taxs += tax_5
      end

      # round
      taxs = (taxs * 10**2).ceil.to_f / 10**2

      mult = count_value * (price_value + taxs)
      mult = (mult * 10**2).ceil.to_f / 10**2
      total += mult
      puts "#{count_value} #{article_value}: #{mult}"
    end
    puts '---------------------------------------------------------------------'
    sum_taxs = (sum_taxs * 10**2).ceil.to_f / 10**2

    puts "Sales Taxes : #{sum_taxs}"
    puts "Total : #{total}"
  end
end

def read_data
  puts '>>>> ENTER LOAD DATA <<<<'
  puts '------------------------------------------------------'
  # Works with hash array
  arr_list = []
  loop do
    print 'ENTER QUANTITY:'
    arr_count = data_reading('count')
    print 'ENTER ARTICLE:'
    arr_article = data_reading('article')
    print 'ENTER PRICE:'
    arr_price = data_reading('price')
    # Add the hash to the array
    arr_list << {
      count: arr_count,
      article: arr_article,
      price: arr_price
    }
    puts '------------------------------------------------------'
    puts 'Enter 0 to finish, else any other number'
    valor = gets.to_i
    next unless valor == 0

    # Do data analysis to return result
    analyze_data(arr_list)
    break
  end
end

read_data
